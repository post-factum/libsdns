#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define SDNS_URI_PREFIX			"sdns://"
#define SDNS_URI_PREFIX_LEN		strlen(SDNS_URI_PREFIX)
#define SDNS_MINIMAL_URI		SDNS_URI_PREFIX"gQA"
#define SDNS_MINIMAL_URI_LEN	strlen(SDNS_MINIMAL_URI)

typedef enum
{
	SDNS_TYPE_PLAIN = 0x00,
	SDNS_TYPE_DNSCRYPT = 0x01,
	SDNS_TYPE_DOH = 0x02,
	SDNS_TYPE_DOT = 0x03,
	SDNS_TYPE_DOQ = 0x04,
	SDNS_TYPE_ODOHT = 0x05,
	SDNS_TYPE_ADNSCRYPT = 0x81,
	SDNS_TYPE_ODOHR = 0x85,
} sdns_type_t;

typedef struct
{
	uint8_t* len;
	unsigned char* val;
} sdns_lp_t;

typedef struct
{
	size_t n;
	sdns_lp_t first;
} sdns_vlp_t;

typedef struct
{
	uint8_t* proto;
	uint64_t* props;
	sdns_lp_t addr;
	sdns_lp_t pk;
	sdns_lp_t prov;
	sdns_vlp_t hashes;
	sdns_lp_t host;
	sdns_lp_t path;
	sdns_vlp_t bips;
} sdns_t;

static unsigned char base64url_decode_table[] =
{
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 62,  0,  0,
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61,  0,  0,  0,  0,  0,  0,
	 0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,  0,  0,  0,  0, 63,
	 0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
};

static size_t __base64url_buf_len(size_t _in_len)
{
	return (_in_len + 1) * 6 / 8;
}

static int __base64url_to_raw(const unsigned char* _in, const size_t _in_len, unsigned char* _buf, size_t _buf_len, size_t* _out_len)
{
	size_t j = 0;

	if (_buf_len < __base64url_buf_len(_in_len))
		return -ENOBUFS;

	for (int i = 0; i < _in_len; )
	{
		uint32_t t = 0;

		for (int k = 3; k >= 0; k--)
			t += (base64url_decode_table[_in[i++]] << k * 6);

		for (int k = 2; k >= 0; k--)
			if (j < _buf_len)
				_buf[j++] = (t >> k * 8) & 0xff;
	}

	*_out_len = j;

	return 0;
}

static int sdns_to_raw(const unsigned char* _uri, unsigned char* _buf, size_t _buf_len, size_t* _out_len)
{
	int uri_len = strlen(_uri);

	if (uri_len < SDNS_MINIMAL_URI_LEN)
		return -ERANGE;

	if (strncmp(_uri, SDNS_URI_PREFIX, SDNS_URI_PREFIX_LEN) != 0)
		return -EPROTONOSUPPORT;

	return __base64url_to_raw(_uri + SDNS_URI_PREFIX_LEN, uri_len - SDNS_URI_PREFIX_LEN, _buf, _buf_len, _out_len);
}

static int sdns_next_field(unsigned char** _buf, size_t _buf_len, size_t* _counter, size_t _step)
{
	*_buf += _step;
	*_counter += _step;
	if (*_counter >= _buf_len)
		return -ERANGE;
	else
		return 0;
}

static int sdns_parse_raw(unsigned char* _buf, size_t _buf_len, sdns_t* _sdns)
{
	size_t parsed = 0;
	int ret;

	_sdns->proto = _buf;
	ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
	if (ret)
		return ret;

	switch (*_sdns->proto)
	{
		case SDNS_TYPE_PLAIN:
		case SDNS_TYPE_DNSCRYPT:
		case SDNS_TYPE_DOH:
		case SDNS_TYPE_DOT:
		case SDNS_TYPE_DOQ:
		case SDNS_TYPE_ODOHT:
		case SDNS_TYPE_ODOHR:
			_sdns->props = (uint64_t*)_buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, 8);
			if (ret)
				return ret;
			break;
	}

	switch (*_sdns->proto)
	{
		case SDNS_TYPE_PLAIN:
		case SDNS_TYPE_DNSCRYPT:
		case SDNS_TYPE_DOH:
		case SDNS_TYPE_DOT:
		case SDNS_TYPE_DOQ:
		case SDNS_TYPE_ADNSCRYPT:
		case SDNS_TYPE_ODOHR:
			_sdns->addr.len = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
			if (ret)
				return ret;

			_sdns->addr.val = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, *_sdns->addr.len);
			if (ret)
				return ret;

			//TODO: separate field for port?
			break;
	}

	switch (*_sdns->proto)
	{
		case SDNS_TYPE_DNSCRYPT:
			_sdns->pk.len = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
			if (ret)
				return ret;

			_sdns->pk.val = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, *_sdns->pk.len);
			if (ret)
				return ret;
			break;
	}

	switch (*_sdns->proto)
	{
		case SDNS_TYPE_DNSCRYPT:
			_sdns->prov.len = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
			if (ret)
				return ret;

			_sdns->prov.val = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, *_sdns->prov.len);
			if (ret)
				return ret;
			break;
	}

	switch (*_sdns->proto)
	{
		case SDNS_TYPE_DOH:
		case SDNS_TYPE_DOT:
		case SDNS_TYPE_DOQ:
		case SDNS_TYPE_ODOHR:
			_sdns->hashes.first.len = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
			if (ret)
				return ret;

			_sdns->hashes.first.val = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, *_sdns->hashes.first.len);
			if (ret)
				return ret;

			_sdns->hashes.n = 1;

			if (!(*_sdns->hashes.first.len & 0x80))
				break;

			for (;;)
			{
				uint8_t* hashn_len = _buf;
				ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
				if (ret)
					return ret;

				ret = sdns_next_field(&_buf, _buf_len, &parsed, *hashn_len & ~0x80);
				if (ret)
					return ret;

				_sdns->hashes.n++;

				if (!(*hashn_len & 0x80))
					break;
			}

			break;
	}

	switch (*_sdns->proto)
	{
		case SDNS_TYPE_DOH:
		case SDNS_TYPE_DOT:
		case SDNS_TYPE_DOQ:
		case SDNS_TYPE_ODOHT:
		case SDNS_TYPE_ODOHR:
			_sdns->host.len = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
			if (ret)
				return ret;

			_sdns->host.val = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, *_sdns->host.len);
			if (ret)
				return ret;

			//TODO: separate field for port?
			break;
	}

	switch (*_sdns->proto)
	{
		case SDNS_TYPE_DOH:
		case SDNS_TYPE_ODOHT:
		case SDNS_TYPE_ODOHR:
			_sdns->path.len = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
			if (ret)
				return ret;

			_sdns->path.val = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, *_sdns->path.len);
			if (ret)
				return ret;
			break;
	}

	switch (*_sdns->proto)
	{
		case SDNS_TYPE_DOH:
		case SDNS_TYPE_DOT:
		case SDNS_TYPE_DOQ:
		case SDNS_TYPE_ODOHR:
			_sdns->bips.first.len = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
			if (ret)
				/* optional field */
				return 0;

			_sdns->bips.first.val = _buf;
			ret = sdns_next_field(&_buf, _buf_len, &parsed, *_sdns->bips.first.len & ~0x80);
			if (ret)
				return ret;

			_sdns->bips.n = 1;

			if (!(*_sdns->bips.first.len & 0x80))
				break;

			for (;;)
			{
				uint8_t* bipn_len = _buf;
				ret = sdns_next_field(&_buf, _buf_len, &parsed, 1);
				if (ret)
					/* optional field */
					return 0;
				ret = sdns_next_field(&_buf, _buf_len, &parsed, *bipn_len & ~0x80);
				if (ret)
					return ret;

				_sdns->bips.n++;

				if (!(*bipn_len & 0x80))
					break;
			}

			break;
	}
}

int main(int _argc, char** _argv)
{
	(void)_argc;
	(void)_argv;

//	const unsigned char uri[] = "sdns://AQcAAAAAAAAAFDE0MC4yMzguMjE1LjE5Mjo4NDQzIH2l4fL6H6BQcKWfdf9ZnrvWxZL_vxKUtQMcWDdZwB6bHjIuZG5zY3J5cHQtY2VydC5wb3N0LWZhY3R1bS50aw";
	const unsigned char uri[] = "sdns://AgcAAAAAAAAAACCcu6D8li96KzHGKxsXWi3kxQqDlXJ-MLYmqAAJp4Dj2BJkb2gucG9zdC1mYWN0dW0udGsKL2Rucy1xdWVyeQ";
	size_t buf_len = __base64url_buf_len(strlen(uri) - SDNS_URI_PREFIX_LEN);
	unsigned char buf[buf_len];
	size_t out_len;
	int res;

	res = sdns_to_raw(uri, buf, buf_len, &out_len);
	printf("res = %d, buf_len=%zu, out_len=%zu\n", res, buf_len, out_len);
//	for (int i = 0; i < out_len; i++)
//		printf("%c", buf[i]);
//	printf("\n");

	sdns_t sdns;
	res = sdns_parse_raw(buf, out_len, &sdns);
	printf("res=%d\n", res);
	printf("proto=%u\n", *sdns.proto);
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
	printf("props=%ju\n", __builtin_bswap64(*sdns.props));
#else
	printf("props=%ju\n", *sdns.props);
#endif
//	printf("addrlen=%u\n", *sdns.addr.len);
//	printf("addr=");
//	for (int i = 0; i < *sdns.addr.len; i++)
//		printf("%c", sdns.addr.val[i]);
//	printf("\n");
//	printf("pklen=%u\n", *sdns.pk.len);
//	printf("pk=");
//	for (int i = 0; i < *sdns.pk.len; i++)
//		printf("%02x", sdns.pk.val[i]);
//	printf("\n");
//	printf("provlen=%u\n", *sdns.prov.len);
//	printf("prov=");
//	for (int i = 0; i < *sdns.prov.len; i++)
//		printf("%c", sdns.prov.val[i]);
//	printf("\n");
	printf("hostlen=%u\n", *sdns.host.len);
	printf("host=");
	for (int i = 0; i < *sdns.host.len; i++)
		printf("%c", sdns.host.val[i]);
	printf("\n");
	printf("pathlen=%u\n", *sdns.path.len);
	printf("path=");
	for (int i = 0; i < *sdns.path.len; i++)
		printf("%c", sdns.path.val[i]);
	printf("\n");
}


